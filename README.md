# gmbinder

Python binding of GMBinder API and CLI tool.

# To install
```bash
# Note: To ommit save to pdf requirements, exclude [PDF]
pip3 install gmbinder[PDF]
```